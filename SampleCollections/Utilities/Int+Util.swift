//
//  Int+Util.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/07.
//  Copyright © 2019 yorihide. All rights reserved.
//

import UIKit

extension Int {
    private func formatString(style: NumberFormatter.Style) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = style
        formatter.locale = Locale(identifier: "ja_JP")
        return formatter.string(from: self as NSNumber) ?? ""
    }
    
    /// 1,234,567 カンマ区切り
    var asJPString: String {    
        return formatString(style: .decimal)
    }
    
    /// ¥1,234,567 カンマ区切り（¥付き）
    var asJPYString: String {
        return formatString(style: .currency)
    }
}
