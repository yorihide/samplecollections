//
//  Date+Util.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/07.
//  Copyright © 2019 yorihide. All rights reserved.
//

import UIKit

public enum FormatterLocale: String {
    case ja = "ja_JP"
    case en = "en_US"
    case enPosix = "en_US_POSIX"
}

extension Date {
    var calendar: Calendar {
        return Calendar(identifier: .gregorian)
    }
    
    /// 年月日時分秒 単位で値を指定してDateを生成. 省略は既存コンポーネント値を引き継ぎ
    init(year: Int? = nil, month: Int? = nil, day: Int? = nil, hour: Int? = nil, minute: Int? = nil, second: Int? = nil) {
        self.init(
            timeIntervalSince1970: Date().fixed(
                year:   year,
                month:  month,
                day:    day,
                hour:   hour,
                minute: minute,
                second: second
                ).timeIntervalSince1970
        )
    }
    
    /// 年月日時分秒 単位で値を修正して返す. 省略は既存コンポーネント値を引き継ぎ
    func fixed(year: Int? = nil, month: Int? = nil, day: Int? = nil, hour: Int? = nil, minute: Int? = nil, second: Int? = nil) -> Date {
        let calendar = self.calendar
        
        var comp = DateComponents()
        comp.year   = year   ?? calendar.component(.year,   from: self)
        comp.month  = month  ?? calendar.component(.month,  from: self)
        comp.day    = day    ?? calendar.component(.day,    from: self)
        comp.hour   = hour   ?? calendar.component(.hour,   from: self)
        comp.minute = minute ?? calendar.component(.minute, from: self)
        comp.second = second ?? calendar.component(.second, from: self)
        
        return calendar.date(from: comp)!
    }
    
    /// 年月日時分秒 単位で値を加算して返す. 省略は既存コンポーネント値を引き継ぎ
    func added(year: Int? = nil, month: Int? = nil, day: Int? = nil, hour: Int? = nil, minute: Int? = nil, second: Int? = nil) -> Date {
        let calendar = self.calendar
        
        var comp = DateComponents()
        comp.year   = (year   ?? 0) + calendar.component(.year,   from: self)
        comp.month  = (month  ?? 0) + calendar.component(.month,  from: self)
        comp.day    = (day    ?? 0) + calendar.component(.day,    from: self)
        comp.hour   = (hour   ?? 0) + calendar.component(.hour,   from: self)
        comp.minute = (minute ?? 0) + calendar.component(.minute, from: self)
        comp.second = (second ?? 0) + calendar.component(.second, from: self)
        
        return calendar.date(from: comp)!
    }
    
    /// Date -> String 変換
    func toString(format: String, locale: FormatterLocale = .enPosix) -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Locale(identifier: locale.rawValue)
        
        return formatter.string(from: self)
    }
    
    /// 曜日を返す
    func weekday() -> String? {
        let i = calendar.component(.weekday, from: self) - 1
        
        return DateFormatter().shortWeekdaySymbols[i]
    }
    
    /// 日時の各コンポーネントを返す
    var year: Int {
        return calendar.component(.year, from: self)
    }
    var month: Int {
        return calendar.component(.month, from: self)
    }
    var day: Int {
        return calendar.component(.day, from: self)
    }
    var hour: Int {
        return calendar.component(.hour, from: self)
    }
    var minute: Int {
        return calendar.component(.minute, from: self)
    }
    var second: Int {
        return calendar.component(.second, from: self)
    }
    
    /// 年齢
    func age() -> Int {
        let now = Int(Date().toString(format: "yyyyMMdd")!)
        let birthday = Int(self.toString(format: "yyyyMMdd")!)
        
        return (now! - birthday!) / 10000
    }
}
