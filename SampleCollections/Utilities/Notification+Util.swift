//
//  Notification+Util.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/07.
//  Copyright © 2019 yorihide. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let name = Notification.Name("name")
}
