//
//  AppDelegate+Util.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/07.
//  Copyright © 2019 yorihide. All rights reserved.
//

import UIKit

class AppDelegate_Util: NSObject {

}
extension AppDelegate {
    /// AppDelegate取得
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    /// root取得
    var rootViewController: RootViewController {
        return window!.rootViewController as! RootViewController
    }
}
