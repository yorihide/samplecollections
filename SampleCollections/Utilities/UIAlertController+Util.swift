//
//  UIAlertController+Util.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/07.
//  Copyright © 2019 yorihide. All rights reserved.
//

import UIKit

extension UIAlertController {
    /// アラートアクション追加
    func addAction(title: String, style: UIAlertAction.Style = .default, handler: ((UIAlertAction) -> Void)? = nil) -> Self {
        let action = UIAlertAction(title: title, style: style, handler: handler)
        addAction(action)
        return self
    }
    
    /// 表示
    func show() {
        AppDelegate.shared.rootViewController.present(self, animated: true, completion: nil)
    }
}
