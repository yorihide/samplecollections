//
//  UIColor+Util.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/07.
//  Copyright © 2019 yorihide. All rights reserved.
//

import UIKit

extension UIColor {
    /// カラーコード -> UIColor
    static func hexString(hex: String, alpha: CGFloat = 1.0) -> UIColor? {
        let hexStr = hex.replacingOccurrences(of: "#", with: "")
        let scanner = Scanner(string: hexStr as String)
        var color: UInt32 = 0
        
        if scanner.scanHexInt32(&color) {
            let r = CGFloat((color & 0xff0000) >> 16) / 255
            let g = CGFloat((color & 0x00ff00) >> 8) / 255
            let b = CGFloat(color & 0x0000ff) / 255
            
            return self.init(red: r, green: g, blue: b, alpha: alpha)
        }
        
        return nil
    }
}
