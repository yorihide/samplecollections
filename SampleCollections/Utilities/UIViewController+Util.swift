//
//  UIViewController+Util.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/07.
//  Copyright © 2019 yorihide. All rights reserved.
//

import UIKit

public enum LeftBarButtonItemType {
    case back
}

public enum RightBarButtonItemType {
    case reload
    case close
}

extension UIViewController {
    func setNavigationBar(title: String, textColor: UIColor = .black, left: [LeftBarButtonItemType]?, right: [RightBarButtonItemType]?) {
        if title.count > 0 {
            setTitle(title: title, textColor: textColor)
        }
        setDefaultBackButton()
        
        if let left = left {
            setLeftBarButton(types: left)
        }
        if let right = right {
            setRightBarButton(types: right)
        }
    }
    
    func setNavigationBar(image: UIImage?, left: [LeftBarButtonItemType]?, right: [RightBarButtonItemType]?) {
        setImageTitle(image: image)
        setDefaultBackButton()

        if let left = left {
            setLeftBarButton(types: left)
        }
        if let right = right {
            setRightBarButton(types: right)
        }
    }
    
    func setTitle(title: String?, textColor: UIColor = .black) {
        let titleLabel = UILabel()
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18.0)
        titleLabel.textColor = textColor
        titleLabel.text = title
        navigationItem.titleView = titleLabel
        titleLabel.sizeToFit()
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.minimumScaleFactor = 0.8
    }
    
    func setImageTitle(image: UIImage?) {
        if let height = navigationController?.navigationBar.frame.height {
            let imageView = UIImageView(image: image)
            imageView.frame.size.height = height
            imageView.contentMode = .scaleAspectFit
            navigationItem.titleView = imageView
        }
    }
    
    private func setDefaultBackButton() {
        let backButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButtonItem
    }
    
    private func setLeftBarButton(types: [LeftBarButtonItemType]) {
        var items = [UIBarButtonItem]()
        for type in types {
            var button: UIBarButtonItem? = nil
            
            switch type {
            case .back:
                if navigationController?.viewControllers.first !== self {
                    button = UIBarButtonItem(image: UIImage(named: "question_btn_return"),
                                             style: .plain,
                                             target: navigationController,
                                             action: #selector(navigationController?.popViewController(animated:)))
                } else {
                    continue
                }
            }
            
            items.append(button!)
        }
        
        navigationItem.leftBarButtonItems = items
    }
    
    private func setRightBarButton(types: [RightBarButtonItemType]) {
        var items = [UIBarButtonItem]()
        for type in types {
            var button: UIBarButtonItem? = nil
            
            switch type {
            case .reload:
                button = UIBarButtonItem(image: UIImage(named: "question_btn_reload"),
                                         style: .plain,
                                         target: self,
                                         action: #selector(showErrorView))
            case .close:
                button = UIBarButtonItem(image: UIImage(named: "question_btn_close"),
                                         style: .plain,
                                         target: self,
                                         action: #selector(showErrorView))
            }
            
            items.append(button!)
        }
        
        navigationItem.rightBarButtonItems = items
    }
    
    @objc func showErrorView() {
        
    }
    
    func frontViewController() -> UIViewController? {
        var root = UIApplication.shared.keyWindow?.rootViewController
        while root?.presentedViewController != nil {
            root = root?.presentedViewController
        }
        
        return root
    }
}
