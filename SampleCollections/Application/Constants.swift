//
//  Constants.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/07.
//  Copyright © 2019 yorihide. All rights reserved.
//

import UIKit

struct Constants {
    struct Environment {
        enum EnvironmentType: String {
            case product = "product"    // 本番
            case stage   = "stage"      // ステージング
            case develop = "develop"    // 開発
        }
        // ビルド環境を返す
        static var type: EnvironmentType {
            let env = Bundle.main.infoDictionary?["Environment"] as! String
            return EnvironmentType(rawValue: env)!
        }
    }
    
    struct Version {
        static var system   = UIDevice.current.systemVersion
        static var app      = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        static var build    = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    }
    
    struct URL {
        struct API {
            static var domain   = ""
            static var base     = ""
        }
    }
    
    struct JWT {
        static var sharedKey    = "xxx"
        static var hs256Header  = ["alg": "HS256", "typ": "JWT"]
        static var issueAtTime  = 30 // JWTを発行してからの有効時間（秒）
    }
}
