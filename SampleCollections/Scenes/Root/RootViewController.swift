//
//  RootViewController.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/07.
//  Copyright © 2019 yorihide. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {
    private var current: UIViewController!
    
    // MARK: - static
    static func instantiate() -> RootViewController {
        return UIStoryboard(name: "Root", bundle: nil).instantiateInitialViewController() as! RootViewController
    }
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        showSplash()
    }
    
    // MARK: - observer
    
    
    // MARK: - func
    /// スプラッシュ画面表示
    func showSplash() {
        current = SplashViewController.instantiate()
        addChild(current)
        current.view.frame = view.bounds
        view.addSubview(current.view)
        current.didMove(toParent: self)
    }
    
    /// Home画面表示
    func showHome() {
        let home = HomeViewController.instantiate()
        switchTo(home)
    }
    
    /// アニメーション付き画面遷移
    func switchWithAnimation(to new: UIViewController, completion: (() -> Void)? = nil) {
        current.willMove(toParent: nil)
        addChild(new)
        
        transition(from: current, to: new, duration: 0.3, options: [.transitionCrossDissolve, .curveEaseOut], animations: {
            
        }) { _ in
            self.current.removeFromParent()
            new.didMove(toParent: self)
            self.current = new
            completion?()
        }
    }
    
    private func switchTo(_ new: UIViewController) {
        addChild(new)
        new.view.frame = view.bounds
        view.addSubview(new.view)
        new.didMove(toParent: self)
        
        current.willMove(toParent: nil)
        current.view.removeFromSuperview()
        current.removeFromParent()
        current = new
    }
}
