//
//  HomeViewController.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/14.
//  Copyright © 2019 yorihide. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    static func instantiate() -> UINavigationController {
        return UIStoryboard(name: "Home", bundle: nil).instantiateInitialViewController() as! UINavigationController
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}
