//
//  LoadingManager.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/08.
//  Copyright © 2019 yorihide. All rights reserved.
//

import UIKit

class LoadingManager: NSObject {
    static var shared = LoadingManager()
    
    var loadingView: UIView!
    
    override init() {
        super.init()
        
        loadingView = UINib(nibName: "LoadingView", bundle: nil).instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    func show(atView: UIView? = nil, opacity: CGFloat = 0.3) {
        var parentView = atView
        if parentView == nil {
            parentView = AppDelegate.shared.rootViewController.view
        }

        loadingView.frame = parentView!.frame
        loadingView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        loadingView.backgroundColor = UIColor.hexString(hex: "000000", alpha: opacity)
        
        loadingView.alpha = 0
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let self = self else { return }
            parentView?.addSubview(self.loadingView)
            self.loadingView.alpha = 1
        }
    }
    
    func dismiss() {
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            guard let self = self else { return }
            self.loadingView.alpha = 0
        }, completion: { [weak self] _ in
            guard let self = self else { return }
            self.loadingView.removeFromSuperview()
            self.loadingView.alpha = 1
        })
    }
}
