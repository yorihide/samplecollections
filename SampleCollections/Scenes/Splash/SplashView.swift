//
//  SplashView.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/12.
//  Copyright © 2019 yorihide. All rights reserved.
//

import UIKit

class SplashView: UIView {
    @IBOutlet weak var logoImageView: UIImageView!
    
    static func instantiate() -> SplashView {
        return UINib(nibName: "SplashView", bundle: nil).instantiate(withOwner: nil, options: nil).first as! SplashView
    }

    override func draw(_ rect: CGRect) {
        logoImageView.image = nil

        var images = [UIImage]()
        
        for i in 1...8 {
            if let image = UIImage(named: "splash\(i)") {
                images.append(image)
            }
        }
        
        logoImageView.animationImages = images
        logoImageView.animationDuration = 1.0
        logoImageView.animationRepeatCount = -1
        
        logoImageView.startAnimating()
    }
}
