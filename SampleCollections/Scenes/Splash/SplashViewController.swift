//
//  SplashViewController.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/14.
//  Copyright © 2019 yorihide. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    @IBOutlet weak var logoImageView: UIImageView!
    
    let duration = 3.0
    
    static func instantiate() -> SplashViewController {
        return UIStoryboard(name: "Splash", bundle: nil).instantiateInitialViewController() as! SplashViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        logoImageView.image = nil
        
        var images = [UIImage]()
        
        for i in 1...8 {
            if let image = UIImage(named: "splash\(i)") {
                images.append(image)
            }
        }
        
        logoImageView.animationImages = images
        logoImageView.animationDuration = 1.0
        logoImageView.animationRepeatCount = -1
        
        logoImageView.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
            let home = HomeViewController.instantiate()
            AppDelegate.shared.rootViewController.switchWithAnimation(to: home)
        }
    }
}
