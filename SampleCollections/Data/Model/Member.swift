//
//  Member.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/08.
//  Copyright © 2019 yorihide. All rights reserved.
//

import Alamofire
import ObjectMapper

struct Member: APIRequestable {
    typealias Response = MemberModel
    
    var path = "v1/member"
    var params = [String: Any]()
    
    init(params: [String: Any]? = nil) {
        if let params = params {
            self.params["param"] = defaultParams.merging(params, uniquingKeysWith: { (old, new) in new })
        } else {
            self.params["param"] = defaultParams
        }
    }
    
    func modelFrom(json: String, urlResponse: HTTPURLResponse) -> (response: MemberModel, header: HeaderModel)? {
        guard let model = Mapper<Response>().map(JSONString: json) else {
            return nil
        }
        
        return (model, model.header)
    }
}

struct MemberModel: Mappable {
    var header: HeaderModel!
    
    var status:         Int!
    var errorMessage:   String?
    var name:           String!
    var age:            Int!
    var gender:         Int!
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        header          <- map["param"]
        
        status          <- map["param.body.status"]
        errorMessage    <- map["param.body.error_message"]
        name            <- map["param.body.name"]
        age             <- map["param.body.age"]
        gender          <- map["param.body.gender"]
    }
}
