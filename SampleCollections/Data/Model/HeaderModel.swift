//
//  HeaderModel.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/08.
//  Copyright © 2019 yorihide. All rights reserved.
//

import ObjectMapper

struct HeaderModel: Mappable {
    var headerStatus:   Int!
    var headerMessage:  String!
    var bodyStatus:     Int!
    var bodyMessage:    String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        headerStatus    <- map["header.status"]
        headerMessage   <- map["header.message"]
        bodyStatus      <- map["body.status"]
        bodyMessage     <- map["body.error_message"]
    }
}
