//
//  APIRequestable.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/08.
//  Copyright © 2019 yorihide. All rights reserved.
//

import Alamofire
import ObjectMapper

protocol APIRequestable {
    associatedtype Response
    
    var method: HTTPMethod              { get }
    var baseURL: String                 { get }
    var path: String                    { get }
    var defaultParams: [String: Any]    { get }
    var params: [String: Any]           { get }
    var requestParams: [String: Any]    { get }
    var headers: [String: String]       { get }
    
    func modelFrom(json: String, urlResponse: HTTPURLResponse) -> (response: Response, header: HeaderModel)?
}

extension APIRequestable {
    var method: HTTPMethod {
        return .post
    }
    
    var baseURL: String {
        return Constants.URL.API.base
    }
    
    var defaultParams: [String: Any] {
        return [
            "client_platform": "ios",
            "client_os_version": Constants.Version.system,
            "client_application_version": Int(Constants.Version.build)!
        ]
    }
    
    var requestParams: [String: Any] {
        return defaultParams.merging(params, uniquingKeysWith: { (old, new) in new })
    }
    
    var headers: [String: String] {
        return ["Content-Type": "application/x-www-form-urlencoded"]
    }
}
