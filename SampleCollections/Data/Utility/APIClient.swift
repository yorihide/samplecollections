//
//  APIClient.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/08.
//  Copyright © 2019 yorihide. All rights reserved.
//

import UIKit
import Alamofire
import RxSwift

public enum APIError: Error {
    case requestFailed                      // リクエスト失敗
    case responseFailed                     // レスポンス取得失敗
    case parseFailed                        // パース失敗
    case headerStatusFailed(Int, String)    // ヘッダーステータスエラー
    case bodyStatusFailed(Int, String?)     // ボディーステータスエラー
    case other                              // その他
    
//    func errorDescription() -> (status: Int, description: String?)? {
//        switch self {
//        case .headerStatusFailed(let info):
//            return (status: info.0, description: info.1)
//        default:
//            return nil
//        }
//    }
}

/*
 // APIClientの使い方
 let member = Member.init(parameters: nil)
 APIClient().call(resource: register)
 .subscribe(onSuccess: { response in
 // 成功
 }, onError: { error in
 // 失敗
 if case APIError.authFailed = error {
 // APIのエラーハンドル
 }
 if let e = error as? APIError, let err = e.originErrorDescription(), err.status == 101 {
 // API固有のエラーハンドル
 }
 })
 .disposed(by: disposeBag)
 */
class APIClient {
    private let timeInterval = 30.0
    private var isShowErrorAlert = false
    
    enum AlertType {
        case normal
        case parse
        case headerStatus
        case bodyStatus
    }
    
    init() {}
    
    /// リクエストインスタンス作成
    private func makeURLRequest<T: APIRequestable>(resource: T) -> URLRequest {
        let url         = URL(string: resource.baseURL + resource.path)!
        let method      = resource.method
        let headers     = resource.headers
        let parameters  = addReservedClaims(parameters: resource.params)
        let jwt         = JWTManager.jwtEncode(header: Constants.JWT.hs256Header,
                                               claims: parameters)
        var urlRequest  = try! URLRequest(url: url, method: method, headers: headers)
        urlRequest.httpBody = jwt?.data(using: .utf8)
        urlRequest.allHTTPHeaderFields = headers
        urlRequest.timeoutInterval = timeInterval
        
        return urlRequest
    }
    
    /// JWTのクレーム要素を追加
    private func addReservedClaims(parameters: [String: Any]) -> [String: Any] {
        let reservedClaims: [String: Any] = ["iat": Int(Date().timeIntervalSince1970)]
        let params = parameters.merging(reservedClaims, uniquingKeysWith: { (old, new) in new })
        printWithJsonFormat(params, isRequest: true)
        
        return params
    }
    
    /// JWTからtokenを取得
    private func responseToken(jwt: String) -> String? {
        guard let jwtData = jwt.data(using: .utf8) else {
            return nil
        }
        guard let jwtDic = try? JSONSerialization.jsonObject(with: jwtData, options: []) as! [String: Any] else {
            return nil
        }
        guard let token = jwtDic["token"] as? String else {
            return nil
        }
        
        return token
    }
    
    /// API呼び出し
    /// isLoading: 通信時のローディング表示
    /// isShowErrorAlert: エラーハンドリングを呼び出し元で行う場合はfalse
    func call<T: APIRequestable>(resource: T, isLoading: Bool = true, isShowErrorAlert: Bool = true) -> Single<T.Response> {
        print("===========================【\(resource.baseURL + resource.path)】===========================")
        
        self.isShowErrorAlert = isShowErrorAlert
        if isLoading { LoadingManager.shared.show() }
        let urlRequest = makeURLRequest(resource: resource)
        return Single.create { singleEvent in
            let request = Alamofire.request(urlRequest).responseString { response in
                if let error = response.result.error {
                    // リクエスト失敗
                    print("【リクエストエラー】\n\(error)")
                    self.errorHandle(alertTitle: "",
                                     alertMessage: "",
                                     isShowAlert: isShowErrorAlert,
                                     errorType: .requestFailed)
                    singleEvent(.error(APIError.requestFailed))
                    
                    return
                }
                guard let jwt = response.result.value, let urlResponse = response.response else {
                    // データの取得失敗
                    print("データ取得失敗")
                    self.errorHandle(alertTitle: "",
                                     alertMessage: "",
                                     isShowAlert: isShowErrorAlert,
                                     errorType: .responseFailed)
                    singleEvent(.error(APIError.responseFailed))
                    
                    return
                }
                print("【ステータスコード】\n\(urlResponse.statusCode)")
                
                // parse
                let json = JWTManager.jwtDecode(jwt: self.responseToken(jwt: jwt) ?? "") ?? ""
                self.printWithJsonFormat(json, isRequest: false)
                guard let model = resource.modelFrom(json: json, urlResponse: urlResponse) else {
                    print("【パース失敗】\n\(jwt)")
                    self.errorHandle(alertTitle: "",
                                     alertMessage: "",
                                     isShowAlert: isShowErrorAlert,
                                     errorType: .parseFailed)
                    singleEvent(.error(APIError.parseFailed))
                    
                    return
                }
                
                // ヘッダーステータス確認
                if model.header.headerStatus != 1 {
                    let error = APIError.headerStatusFailed(model.header.headerStatus, model.header.headerMessage)
                    self.errorHandle(isShowAlert: isShowErrorAlert, errorType: error)
                    singleEvent(.error(error))
                    
                    return
                }
                
                // ボディーステータス確認
                if model.header.bodyStatus != 1 {
                    let error = APIError.bodyStatusFailed(model.header.bodyStatus, model.header.bodyMessage)
                    self.errorHandle(isShowAlert: isShowErrorAlert, errorType: error)
                    singleEvent(.error(error))
                    
                    return
                }
                
                singleEvent(.success(model.response))
                LoadingManager.shared.dismiss()
            }
            
            return Disposables.create { request.cancel() }
        }
    }
    
    /// APIエラーハンドリング
    private func errorHandle(alertTitle: String? = nil, alertMessage: String? = nil, isShowAlert: Bool, errorType: APIError) {
        LoadingManager.shared.dismiss()
        if !isShowAlert { return }
        
        switch errorType {
        case .requestFailed:
            // リクエスト失敗
            showAlert(title: alertTitle, message: alertMessage)
        case .responseFailed:
            // レスポンス取得失敗
            showAlert(title: alertTitle, message: alertMessage)
        case .parseFailed:
            // パース失敗
            showAlert(title: alertTitle, message: alertMessage)
        case let .headerStatusFailed(status, desc):
            // ヘッダーステータスエラー
            handleHeader(status: status, desc: desc)
        case let .bodyStatusFailed(status, desc):
            // ボディーステータスエラー
            handleBody(status: status, desc: desc)
        case .other:
            // その他
            print("")
        }
    }
    
    /// ヘッダーステータスエラーのハンドリング
    private func handleHeader(status: Int, desc: String) {
        if status == 101 {
            
        } else if status == 102 {
            
        }
    }
    
    /// ボディーステータスエラーのハンドリング
    private func handleBody(status: Int, desc: String?) {
        if status == 101 {
            
        } else if status == 102 {
            
        }
    }
    
    /// アラートを表示するのみ
    private func showAlert(title: String?, message: String?) {
        guard let title = title, let message = message else { return }
        UIAlertController(title: title, message: message, preferredStyle: .alert)
        .addAction(title: "OK")
        .show()
    }
    
    /// デバッグ用パラメータログ表示
    private func printWithJsonFormat(_ params: Any, isRequest: Bool) {
        var object = params
        if object is String {
            let json = params as! String
            let data = json.data(using: .utf8)!
            if let dic = try? JSONSerialization.jsonObject(with: data, options: []) {
                object = dic
            } else {
                return
            }
        }
        
        let data = try? JSONSerialization.data(withJSONObject: object, options: .prettyPrinted)
        if let data = data, let json = String(bytes: data, encoding: .utf8) {
            let message = isRequest ? "【リクエストパラメータ】" : "【レスポンスパラメータ】"
            print("\(message)\n\(json)")
        }
    }
}
