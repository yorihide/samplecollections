//
//  JWTManager.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/07.
//  Copyright © 2019 yorihide. All rights reserved.
//

import CryptoSwift

struct JWTModel {
    var header: [String: Any]
    var claims: [String: Any]
    
    init(header: [String: Any], claims: [String: Any]) {
        self.header = header
        self.claims = claims
    }
}

class JWTManager {
    /// 共有鍵生成
    private static func makeSharedKey() -> String {
        if let key = DataManager.jwtSharedKey {
            // 固有共有鍵
            print("key:\(key)")
            return key
        }
        
        // 共有秘密鍵
        let hash = Constants.JWT.sharedKey.sha256()
        let start = hash.index(hash.startIndex, offsetBy:6)
        let end = hash.index(hash.startIndex, offsetBy: 37)
        let key = String(hash[start...end])
        print("key:\(key)")
        
        return key
    }
    
    /// JWT encode
    static func jwtEncode(header: [String: Any], claims: [String: Any]) -> String? {
        // header
        guard let header = makeHeader(header: header) else {
            return nil
        }
        // claims
        guard let claims = makeClaims(claims: claims) else {
            return nil
        }
        // signature
        guard let signature = makeSignature(header: header, claims: claims) else {
            return nil
        }
        
        //        print("token=" + header + "." + claims + "." + signature)
        return "token=" + header + "." + claims + "." + signature
    }
    
    private static func makeHeader(header: [String: Any]) -> String? {
        guard let headerData = try? JSONSerialization.data(withJSONObject: header, options: []) else {
            return nil
        }
        return base64URLEncode(headerData)
    }
    
    private static func makeClaims(claims: [String: Any]) -> String? {
        guard let claimsData = try? JSONSerialization.data(withJSONObject: claims, options: []) else {
            return nil
        }
        return base64URLEncode(claimsData)
    }
    
    static func makeSignature(header: String, claims: String) -> String? {
        let key = makeSharedKey()
        let joinedStr = header + "." + claims
        let bytes: [UInt8] = joinedStr.bytes
        if let hmac = try? HMAC(key: key, variant: .sha256).authenticate(bytes) {
            return base64URLEncode(Data(bytes: hmac))
        } else {
            return nil
        }
    }
    
    /// JWT decode
    static func jwtDecode(jwt: String) -> String? {
        if !verify(jwt: jwt) {
            print("JWT改ざん検知")
            return nil
        }
        guard let jwtModel = jwtSerialize(jwt: jwt) else {
            print("JWTシリアライズエラー")
            return nil
        }
        if !validate(model: jwtModel) {
            print("JWTバリデートエラー")
            return nil
        }
        
        let claims = jwtModel.claims
        guard let jsonData = try? JSONSerialization.data(withJSONObject: claims, options: []) else {
            return nil
        }
        
        return String(bytes: jsonData, encoding: .utf8)
    }
    
    private static func jwtSerialize(jwt: String) -> JWTModel? {
        let components = jwt.components(separatedBy: ".")
        guard components.count == 2 || components.count == 3,
            let headerData = base64URLDecode(components[0]),
            let claimsData = base64URLDecode(components[1]),
            let header = (try? JSONSerialization.jsonObject(with: headerData)) as? [String:Any],
            let claims = (try? JSONSerialization.jsonObject(with: claimsData)) as? [String:Any] else {
                return nil
        }
        
        return JWTModel.init(header: header, claims: claims)
    }
    
    /// 署名確認
    private static func verify(jwt: String) -> Bool {
        let components = jwt.components(separatedBy: ".")
        if components.count != 3 {
            return false
        }
        let header = components[0]
        let claims = components[1]
        let signature = components[2]
        
        let signature_ = makeSignature(header: header, claims: claims)
        return signature == signature_
    }
    
    private static func validate(model: JWTModel) -> Bool {
        let header = model.header
        
        // header
        // アルゴリズム
        if let alg = header["alg"] as? String, alg != "HS256" {
            print("alg validate error.")
            return false
        }
        // タイプ
        if let typ = header["typ"] as? String, typ != "JWT" {
            print("typ validate error.")
            return false
        }
        
        // claims
        let claims = model.claims
        let unixtime = Int(Date().timeIntervalSince1970)
        // 発行日時
        guard let iat = claims["iat"] as? Int else {
            print("iatパラメータ無し")
            return false
        }
        if abs(iat - unixtime) > Constants.JWT.issueAtTime {
            print("JWT発行日時不正")
            // レスポンスステータスで時間不正判定するため処理は続行させる
            //            return false
        }
        
        return true
    }
    
    /// base64 url encode
    private static func base64URLEncode(_ data: Data) -> String? {
        let base64EncodedData = data.base64EncodedData()
        if let base64EncodedString = String(data: base64EncodedData, encoding: .utf8) {
            let base64URLEncodedString = base64EncodedString
                .replacingOccurrences(of: "+", with: "-")
                .replacingOccurrences(of: "/", with: "_")
                .replacingOccurrences(of: "=", with: "")
            return base64URLEncodedString
        }
        return nil
    }
    
    /// base64 url decode
    private static func base64URLDecode(_ base64URLEncodedString: String) -> Data? {
        #if swift(>=4.0)
        let paddingLength = 4 - base64URLEncodedString.count % 4
        #else
        let paddingLength = 4 - base64URLEncodedString.characters.count % 4
        #endif
        let padding = (paddingLength < 4) ? String(repeating: "=", count: paddingLength) : ""
        
        let base64EncodedString = base64URLEncodedString
            .replacingOccurrences(of: "-", with: "+")
            .replacingOccurrences(of: "_", with: "/")
            + padding
        
        return Data(base64Encoded: base64EncodedString)
    }
}
