//
//  DataManager.swift
//  SampleCollections
//
//  Created by ynoda on 2019/03/07.
//  Copyright © 2019 yorihide. All rights reserved.
//

import UIKit
import KeychainAccess

class DataManager: NSObject {
    private static let defaults = UserDefaults.standard
    private static let keychain = Keychain()
    
    /// keychain全削除
    static func keychainRemoveAll() {
        do {
            try keychain.removeAll()
        } catch {
            print(error)
        }
    }
    
    /// アクティベートフラグ
    static var isActivated: Bool {
        get {
            return defaults.bool(forKey: "IsActivated")
        }
        set {
            defaults.set(newValue, forKey: "IsActivated")
        }
    }
    
    /// JWT共通鍵
    static var jwtSharedKey: String? {
        get {
            do {
                return try keychain.get("JwtSharedKey")
            } catch let error {
                print(error)
                return nil
            }
        }
        set {
            keychain["JwtSharedKey"] = newValue
        }
    }
}
